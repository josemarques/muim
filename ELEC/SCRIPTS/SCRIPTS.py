import pcbnew
import os

def HIDALLREF():#Hide all references on PCB
	PCB = pcbnew.GetBoard()
	if hasattr(PCB, 'GetModules'):
		MODULES = PCB.GetModules()
	else:
		MODULES = PCB.GetFootprints()
	for MODULE in MODULES:
		MODULE.Reference().SetVisible(False)
