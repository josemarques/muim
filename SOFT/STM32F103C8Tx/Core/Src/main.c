/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/*
#include "BITMAP.h"
#include "SSD1306.h"
*/

#include "ssd1306/ssd1306.h"
#include "ssd1306/ssd1306_tests.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define BT_UP GPIO_PIN_3
#define BT_RG GPIO_PIN_4
#define BT_LF GPIO_PIN_5
#define BT_DW GPIO_PIN_6
#define BT_IN GPIO_PIN_7

#define LD_RT GPIO_PIN_13 //PB13
#define LD_LT GPIO_PIN_12 //PB12
#define LD_RD GPIO_PIN_14 //PC14
#define LD_LD GPIO_PIN_13 //PC13

#define POW_CTRL GPIO_PIN_15 //PB15

bool DP_UP=0;
bool DP_DW=0;
bool DP_RG=0;
bool DP_LF=0;
bool DP_IN=0;

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
 ADC_HandleTypeDef hadc1;

I2C_HandleTypeDef hi2c1;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
static void MX_I2C1_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_I2C1_Init();
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN 2 */

  //if(tC((0x3C << 1))){
//	  DP_UP=1;
 // }
 // else{
	 // DP_UP=0;
  //}

  ssd1306_Init();
  //ssd1306_TestAll();
//ssd1306_TestFonts();
  //HAL_Delay(300);
  //ssd1306_TestFPS();
  //HAL_Delay(300);
  //HAL_Delay(300);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  int xDP=0;
  int yDP=0;
  int zDP=0;

  int pxDP=0;
  int pyDP=0;
  int pzDP=0;

  uint8_t MENU=0;

  uint32_t STARTTIME = HAL_GetTick();
  uint32_t ONTIME = STARTTIME;
  uint32_t TIMER = STARTTIME;

  while (1)  {

	  ssd1306_Fill(Black);//Clean screen
	  char TEXTLINE[22];
	  //sprintf(TEXTLINE, "XDP=%d; YDP=%d; ZDP=%d", xDP,yDP,zDP);
	  TEXTLINE[0] = '\0';
	  sprintf(TEXTLINE, "XDP=%d", xDP);
	  ssd1306_SetCursor(2, 0);
	  ssd1306_WriteString(TEXTLINE, Font_6x8, White);

	  TEXTLINE[0] = '\0';
	  sprintf(TEXTLINE, "YDP=%d", yDP);
	  ssd1306_SetCursor(44, 0);
	  ssd1306_WriteString(TEXTLINE, Font_6x8, White);

	  TEXTLINE[0] = '\0';
	  sprintf(TEXTLINE, "ZDP=%d", zDP);
	  ssd1306_SetCursor(86, 0);
	  ssd1306_WriteString(TEXTLINE, Font_6x8, White);

	  if(pyDP==0&&yDP==1){
		  MENU++;
	  }
	  else if(pyDP==0&&yDP==-1){
		  MENU--;
	  }

	  if (MENU==0){

	  }
	  else if(MENU==1){
		  TEXTLINE[0] = '\0';
		  sprintf(TEXTLINE, "Press to turn off", ONTIME);

		  ssd1306_SetCursor(2, 8);
		  ssd1306_WriteString(TEXTLINE, Font_6x8, White);
		  if(pzDP==-1&&zDP==0){
			  GPIOB -> ODR &= ~POW_CTRL;//Sets PIN to 0
		  }
	  }

	  else if(MENU==2){
	  ONTIME = HAL_GetTick();
	  TEXTLINE[0] = '\0';
	  	  sprintf(TEXTLINE, "TICKS=%d", ONTIME);

	  	  ssd1306_SetCursor(2, 8);
	  	  ssd1306_WriteString(TEXTLINE, Font_6x8, White);

	  if(ONTIME>=1800*1000){// At 1800 seconds turn off
		  GPIOB -> ODR &= ~POW_CTRL;//Sets PIN to 0
	  }
	  }

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */


	  //memset(TEXTLINE,0,sizeof(buffer));
	  //memset(TEXTLINE,0,strlen(buffer));


	  ssd1306_UpdateScreen();


	  bool DP_UP=(GPIOA -> IDR & BT_UP);
	  bool DP_DW=(GPIOA -> IDR & BT_DW);
	  bool DP_RG=(GPIOA -> IDR & BT_RG);
	  bool DP_LF=(GPIOA -> IDR & BT_LF);
	  bool DP_IN=(GPIOA -> IDR & BT_IN);
	  //*/

	  /*
#define LD_RT GPIO_PIN_13 //PB13
#define LD_LT GPIO_PIN_12 //PB12
#define LD_RD GPIO_PIN_14 //PC14
#define LD_LD GPIO_PIN_13 //PC13
*/
	  pxDP=xDP;
	  pyDP=yDP;
	  pzDP=zDP;

	  if(DP_UP){
		  yDP=1;
		  GPIOB -> ODR |= LD_RT;//Sets PIN to 1
		  GPIOB -> ODR |= LD_LT;//Sets PIN to 1
		  GPIOC -> ODR &= ~LD_RD;//Sets PIN to 0
		  GPIOC -> ODR &= ~LD_LD;//Sets PIN to 0
	  }
	  else if(DP_DW){
		  yDP=-1;
		  GPIOB -> ODR &= ~LD_RT;//Sets PIN to 0
		  GPIOB -> ODR &= ~LD_LT;//Sets PIN to 0
		  GPIOC -> ODR |= LD_RD;//Sets PIN to 1
		  GPIOC -> ODR |= LD_LD;//Sets PIN to 1
	  }
	  else {
		  yDP=0;
	  }
	  if(DP_RG){
		  xDP=1;
	  	  GPIOB -> ODR |= LD_RT;//Sets PIN to 0
	  	  GPIOB -> ODR &= ~LD_LT;//Sets PIN to 1
	  	  GPIOC -> ODR |= LD_RD;//Sets PIN to 0
	  	  GPIOC -> ODR &= ~LD_LD;//Sets PIN to 0
	  }
	  else if(DP_LF){
		  xDP=-1;
	  	  GPIOB -> ODR &= ~LD_RT;//Sets PIN to 1
	  	  GPIOB -> ODR |= LD_LT;//Sets PIN to 0
	  	  GPIOC -> ODR &= ~LD_RD;//Sets PIN to 1
	  	  GPIOC -> ODR |= LD_LD;//Sets PIN to 0
	  }
	  else {
		  xDP=0;
	  }
	  if(DP_IN){
		  zDP=-1;
	  	  GPIOB -> ODR |= LD_RT;//Sets PIN to 1
	  	  GPIOB -> ODR |= LD_LT;//Sets PIN to 1
	  	  GPIOC -> ODR |= LD_RD;//Sets PIN to 1
	  	  GPIOC -> ODR |= LD_LD;//Sets PIN to 1
	  }
	  else {
		  zDP=0;
		 /* GPIOC -> ODR &= ~LD_RT;//Sets PIN to 1
		  GPIOC -> ODR &= ~LD_LT;//Sets PIN to 0
		  GPIOB -> ODR &= ~LD_RD;//Sets PIN to 1
		  GPIOB -> ODR &= ~LD_LD;//Sets PIN to 0*/
	  }




	     /*
	     GPIOB -> ODR |= DCOAC_ECLK;//Sets PIN to 1
	     GPIOB -> ODR &= ~DCOAC_ECLK;//Sets PIN to 0
	     /*
	     /*
	     bool rcvbit=(GPIOB -> IDR & DCOAC_TX);
	     bool rcvbitN=(GPIOB -> IDR & DCOAC_ICLK);
	    */

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC|RCC_PERIPHCLK_USB;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */

  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 400000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, SYS_LED_2_Pin|SYS_LED_3_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, DCOAC_RX_Pin|DCOAC_ECLK_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, SYS_LED_0_Pin|SYS_LED_1_Pin|POW_CNTR_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, AC__Pin|AC_A9_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : SYS_LED_2_Pin SYS_LED_3_Pin */
  GPIO_InitStruct.Pin = SYS_LED_2_Pin|SYS_LED_3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : B_A_Pin B_B_Pin B_C_Pin B_D_Pin
                           B_INT_Pin */
  GPIO_InitStruct.Pin = B_A_Pin|B_B_Pin|B_C_Pin|B_D_Pin
                          |B_INT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : DCOAC_RX_Pin DCOAC_ECLK_Pin */
  GPIO_InitStruct.Pin = DCOAC_RX_Pin|DCOAC_ECLK_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_MEDIUM;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : DCOAC_ICLK_Pin DCOAC_TX_Pin */
  GPIO_InitStruct.Pin = DCOAC_ICLK_Pin|DCOAC_TX_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : SYS_LED_0_Pin SYS_LED_1_Pin */
  GPIO_InitStruct.Pin = SYS_LED_0_Pin|SYS_LED_1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : POW_CNTR_Pin */
  GPIO_InitStruct.Pin = POW_CNTR_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(POW_CNTR_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : AC__Pin AC_A9_Pin */
  GPIO_InitStruct.Pin = AC__Pin|AC_A9_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
