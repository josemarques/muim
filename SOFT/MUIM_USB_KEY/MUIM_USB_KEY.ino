#include <HID.h>

#include <Arduino.h>//Librería de arduino, definiciones generales

#include <EEPROM.h>


#include <Mouse.h>
#include <Keyboard.h>

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2

#define LOGO16_GLCD_HEIGHT 16 
#define LOGO16_GLCD_WIDTH  16 

#if (SSD1306_LCDHEIGHT != 32)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

//screen
//Screen saver

int DEPLTRND(){
  if(random(0,16)>6){
    return random(0,255);
    }
    else {
      return 0;}
    
    return 0;
  }


String mtrx0="                     ";
String mtrx1="                     ";
String mtrx2="                     ";
String mtrx3="                     ";
String mtrx4="                     ";
String mtrx5="                     ";

int m0p=5*6;
int m1p=4*6;
int m2p=3*6;
int m3p=2*6;
int m4p=1*6;
int m5p=0;

void MTRX(){

//Propagate chars
m0p++;
m1p++;
m2p++;
m3p++;
m4p++;
m5p++;

if(m0p>(4+2)*6){
  for (int i=0; i<21;i++){
         mtrx0.setCharAt(i+1,(char)DEPLTRND()); 
         }
  m0p=0;
  }
  if(m1p>(4+2)*6){
  for (int i=0; i<21;i++){
         mtrx1.setCharAt(i+1,(char)DEPLTRND()); 
         }  
  m1p=0;
  }
  if(m2p>(4+2)*6){
    for (int i=0; i<21;i++){
         mtrx2.setCharAt(i+1,(char)DEPLTRND()); 
         }
  m2p=0;
  }
  if(m3p>(4+2)*6){
    for (int i=0; i<21;i++){
         mtrx3.setCharAt(i+1,(char)DEPLTRND()); 
         }
  m3p=0;
  }
    if(m4p>(4+2)*6){
    for (int i=0; i<21;i++){
         mtrx4.setCharAt(i+1,(char)DEPLTRND()); 
         }
  m4p=0;
  }
    if(m5p>(4+2)*6){
    for (int i=0; i<21;i++){
         mtrx5.setCharAt(i+1,(char)DEPLTRND()); 
         }
  m5p=0;
  }


// Generate chars

  
display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,m0p-6);
  display.println(mtrx0);
  display.setCursor(0,m1p-6);
  display.println(mtrx1);
  display.setCursor(0,m2p-6);
  display.println(mtrx2);
  display.setCursor(0,m3p-6);
  display.println(mtrx3);
  display.setCursor(0,m4p-6);
  display.println(mtrx4);
  display.setCursor(0,m5p-6);
  display.println(mtrx5);
         
  
  }
//Screen

//hardware setup
const int SLED = 17; // LED connected to digital pin 13
const int LB = 21;   // pushbutton connected to digital pin 8//Left
const int RB = 20;   // pushbutton connected to digital pin 9//Rigth
const int CB = 19;   // pushbutton connected to digital pin 6//Enter

const int E_VR=6;

const int N_FF=8;


//Button read functions and intermidiate variables
bool isPLB=false;
bool wsPLB=false;
bool isPRB=false;
bool wsPRB=false;
bool isPCB=false;
bool wsPCB=false;

int tLBp_1=0;//tem left button number of presses.
int tRBp_1=0;//""""
int tCBp_1=0;//""""

int tLBp_2=0;//tem left button number of presses.
int tRBp_2=0;//""""
int tCBp_2=0;//""""

bool single_release(int button, bool *isP, bool *wsP){
  if(*isP){
      *wsP=true;
      }
  else{
      *wsP=false;
   }
   
  if(digitalRead(button)){
    *isP=false;

    if(*wsP){
    return true;}
    else {
    return false;}
  }
  
  else if(!digitalRead(button)){
    *isP=true;
    return false;
   }
  }

bool single_press(int button, bool *isP, bool *wsP){
  if(*isP){
      *wsP=true;
      }
  else{
      *wsP=false;
   }
   
  if(digitalRead(button)){
    *isP=false;
    return false;
  }
  
  else if(!digitalRead(button)){
    *isP=true;
    if(!*wsP){
    return true;}
    else {
    return false;}
   }
  }
//Button read functions and intermidiate variables





//hardwaresetup


//nput mode

int MODE=0;//mode of operation
int LMODE=0;
//Variables for mode 1
char ETIQ [7][11]={"PASS0", "PASS1","PASS2","PASS3","PASS4","PASS5","PASS6"};
char KEYS [7][25]={"PAWRD0","PAWRD1","PAWRD2","PAWRD3","PAWRD4","PAWRD5","PAWRD6"};
int CK=0;//Current selected slot key

//input mode



//EEPROM
int address = 0;
byte value;
//EEPROM







//Timing interrupt

//Definición de variables para el control de la velocidad de instrucciones temporizadas
volatile int T1C;//Preload timer 65536-16MHz;256-2Hz;
volatile long TIC;//Acumulador del timer general
//Definición de variables para el control de la velocidad de instrucciones temporizadas

volatile unsigned int RTEX_1=0;
volatile unsigned int RTEX_2=0;

float TIM_BI=1;//Time between interrupts, in seconds; 1[s]>=TIM_BI[s]>=(2)*((10)^(-5))[s]
ISR(TIMER1_OVF_vect){//Rutina de interrupción. definida en <Arduino.h>
  T1C=65536-((16000000*TIM_BI)/(256));//Regulación de velocidad
  TCNT1 = T1C;//Renovcación de la precarga


RTEX_1++;
RTEX_2++;
  
 
}//*/




int vREF=0;
float VREF=2.5;
float VDD=0;

void setup() {

//ETIQ={"MRQ_SRV-CR", "MRQ_SRV" ,"MRQ_SRV_PI", "AXIS_SRV", "MRW"};

  
  pinMode(SLED, OUTPUT);      // sets the digital pin 13 as output
  pinMode(LB, INPUT);      // sets the digital pin 7 as input
  pinMode(RB, INPUT);      // sets the digital pin 7 as input
  pinMode(CB, INPUT);      // sets the digital pin 7 as input

  /*pinMode(N_FF, OUTPUT);      // sets the digital pin 7 as input
  digitalWrite(N_FF,0);*/

  pinMode(E_VR, OUTPUT);      // sets the digital pin 7 as input
  digitalWrite(E_VR,1);
  
  Keyboard.begin();
    // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3C (for the 128x32)
  // init done
    display.clearDisplay();
  display.setTextSize(4);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println(" MUIM");
  display.display();
    delay(100);
   // display.clearDisplay();




//Inicialización de TMR1
    TCCR1A = 0;
    TCCR1B = 0;

    T1C = 65444;//Declaración de la precarga inicial.
  
    TCNT1 = T1C;//Precarga inicial.
    TCCR1B |= (1 << CS12);    // 256 prescaler 
    TIMSK1 |= (1 << TOIE1);   // Habilitación de interrupciones por overflow
    
    interrupts();//Habilitación de las interrupciones+
    //Inicialización de TMR1

    //TIMSK0 &= ~(1 << TOIE0);   //  Disable Timer 0 interrupt para estabilizar las lecturas



vREF=analogRead(A5);  // READ 2.5V reference voltage.
VDD=((float)1023/((float)vREF))*(float)VREF*(float)1;//*/
    
}

void loop() {

//Input processing
  if(single_press(LB,&isPLB,&wsPLB)){
    tLBp_1=tLBp_1+1;//temp left button number of presses.
    tLBp_2=tLBp_2+1;
    LMODE=MODE;
    MODE=0;
    
 }
  if(single_press(RB,&isPRB,&wsPRB)){
    tRBp_1=tRBp_1+1;//""""
    tRBp_2=tRBp_2+1;
    LMODE=MODE;
    MODE=0;
  }
      
  if(single_press(CB,&isPCB,&wsPCB)){
    tCBp_1=tCBp_1+1;//""""
    tCBp_2=tCBp_2+1;//""""
    LMODE=MODE;
    MODE=0;
  } 
       
//Input processing
/*


*/
if(RTEX_2>1){
RTEX_2=0;

}

  if(RTEX_1>10){
RTEX_1=0;

if(tLBp_2==0&&tRBp_2==0&&tCBp_2==0){
  LMODE=MODE;
  MODE=1;
  }
  else{
    //LMODE=MODE;
    //MODE=0;//BUG HERE
   }
    
vREF=analogRead(A5);  // READ 2.5V reference voltage.
VDD=((float)1023/((float)vREF))*(float)VREF*(float)1;//


tLBp_2=0;//templeft button number of presses.
tRBp_2=0;//""""
tCBp_2=0;//*/

  }

  
if(MODE==-1){//Debug mode
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  
  display.print("tLBp_1:");
  display.print(tLBp_1);

  display.print(" tRBp_1:");
  display.print(tRBp_1);

  display.print(" tCBp_1:");
  display.print(tCBp_1);

  display.println();

    display.print("tLBp_2:");
  display.print(tLBp_2);

  display.print(" tRBp_2:");
  display.print(tRBp_2);

  display.print(" tCBp_2:");
  display.print(tCBp_2);

  display.println();

   display.display();
  display.clearDisplay();
  
  }


else if(MODE==0){

  if(LMODE!=MODE){
    tCBp_1=0;
    tLBp_1=0;
    tRBp_1=0;
    }

if((tCBp_1>0)){
  tCBp_1=0;
   Keyboard.print(KEYS[CK]);
  delay(150);
Keyboard.press(KEY_RETURN);
Keyboard.releaseAll();//*/
  }//


if(tLBp_1>0){
  CK=CK-1;
  tLBp_1=0; 
  }

  if(tRBp_1>0){
    CK=CK+1;
    tRBp_1=0;
    }

    
if(CK>6){
        CK=0;
      }
       else if(CK<0){
        CK=6;
      }
/*

  */


  
display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);

        display.print("MODE:");
        display.print(MODE);
        display.print(" DBG:");
        display.print(tLBp_1);
        display.print(tRBp_1);
        display.print(tCBp_1);
        display.print(tLBp_2);
        display.print(tRBp_2);
        display.print(tCBp_2);
        display.println(" |||");
        
        
        
  display.setTextSize(2);
  
    display.println(ETIQ[CK]);

display.setTextSize(1);
display.print("SYS VOLT:");
display.println(VDD);
        

        
 display.display();
  display.clearDisplay();

   
}    
else if(MODE==1){
    if(tLBp_1||tRBp_1||tCBp_1){
      tLBp_1=0;
      tRBp_1=0;
      tCBp_1=0;
      }
MTRX(); 

 
 display.display();
 display.clearDisplay();
         
 }
else{//
  


display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
        display.println("MODE ERROR");
        
display.print("MODE: ");
display.print(MODE);
        
 display.display();
  display.clearDisplay();

       
  }



}
